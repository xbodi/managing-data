---
title: "analyse_data_forage_bellecombe_aval_2020_2021"
author: "Xavier Bodin"
date: "25/03/2022"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

## Data

Il s'agit des données transmises par JMK sur le forage aval, sur l'année 2020-2021
```{r}
a <- read.csv('/home/xavier/DATA/ALPES/SITES/ECRINS/DEUX_ALPES/Forages Bellecombes/Forage aval (SD1)/Datas_forage_aval_01062021.csv', sep = ";", dec = ",", header = T)
a$Date <- as.Date(a$Date, format = "%d/%m/%Y %H:%M")
depths <- c(0.2, 0.6, 1.1, 1.6, 2.6, 3.6, 4.6, 5.6, 6.6, 7.6, 8.6, 10.1, 12.1, 13.6)
```


## Plots


```{r, echo = FALSE}
col <- rainbow(ncol(a)-1)

plot(a$Date, a$X0.2m, col = "darkblue", xlab =NA, ylab = "Temperature [°C]", xaxt = "n", type = "n")
axis.Date(side = 1, at = seq(as.Date("2020-06-01"), as.Date("2021-09-01"), "3 month"), format = "%b-%y", tcl = -0.8)
axis.Date(side = 1, at = seq(as.Date("2020-06-01"), as.Date("2021-09-01"), "month"), labels = FALSE, tcl = -0.4)
abline(v = seq(as.Date("2020-06-01"), as.Date("2021-09-01"), "3 month"), lty = 2, lwd = 0.5)

for(i in 2 : ncol(a)){
  lines(a$Date, a[,i], col = col[i])
}

abline(h=0)
legend("topright", legend = paste(depths, "m"), lty = 1, col = col, ncol = 2, bty = "n")

```

