# PermaFrance - Description of the Rock Glacier Velocity (RGV) protocols

The present documentation is a local (for the French observatory of permafrost PermaFrance) implementation of the documents produced by the IPA Action group Rock Glaciers Inventories and Kinematics. The basis documents of the Task 1 of the RGIK *"Kinematics as an optional attribute in standardized rock glacier inventories"* are the following :

-   [Baseline concepts (v3.0.1) [22.07.2022]](https://bigweb.unifr.ch/Science/Geosciences/Geomorphology/Pub/Website/IPA/CurrentVersion/Current_KinematicalAttribute.pdf)

-   [Practical guidelines: rock glacier inventory using InSAR (kinematic approach) (v3.0.2) [11.06.2020]](https://bigweb.unifr.ch/Science/Geosciences/Geomorphology/Pub/Website/CCI/CurrentVersion/Current_InSAR-based_Guidelines.pdf)

Based on those documents, the protocols for acquiring and processing data on rock glacier velocity are described below.

## 1. Acquisition phase

This phase refers to the time when measurements are carried out on the field, with a dedicated GNSS device handled by an operator. Each site has a specific sampling design (number of marked blocks, distribution in space), constrained by the terrain and by the necessity of achieving the GNSS in a single day.

### 1.1 Description of the devices

Within the PermaFrance groups, we use the following Differential GNSS devices[^permafrance_protocole_rgv_rock_glaciers_velocity-1] :

[^permafrance_protocole_rgv_rock_glaciers_velocity-1]: add links to technical specification and manuals of the devices

-   at EDYTEM: Topcon GB1000 (as a base station) + Trimble GEO7X (as a rover) since \~2010 + Trimble R8s since 2022 (as a RTK)
-   at PACTE: 2 x Trimble GEO7X (base + rover)

As shown on the photo below, a 2-m pole and a bipode is generally used for the rover, and the target is materialized on the ground with a painted cross.

![*On the left: The PG-A1 antena of the Topcon GB1000 base station installed on an alu 13-cm pole with a 5/8' geodetic thread. On the right: pole and bipode for measuring points with a Trimble Geo7x GNSS rover.*](RGV_GNSS_field_acquisition_2000px.jpg)

### 1.2 Collecting data

Annual field campaign, generally done in late summer for ensuring the absence of snow (that may cover marked blocks).

Around 10 to 50 (depending on the site and the mood of the one that initially setup the survey) marked blocks are measured during each campaign, using a base station (`/PermaFrance/DONNEES/1_DATA_MANAGEMENT/COORDONNEES_BASE_GPS.txt`) and a rover station (2-m high pole + bipode).

## 2. Processing phase

### 2.1 From the field to the hard-drive

#### Procedure with Topcon GB1000 GNSS

Voir [ce petit tuto](/PermaFrance/DONNEES/1_DATA_MANAGEMENT/GITLAB_OSUG/managing-permafrance-data/GESTION/GESTION_GPS/TOOLS/trucs%20IP.txt).

1.  Raw files stored in `PermaFRANCE/CAMPAGNES/YYYY/SITE/DD_MONTH_YYYY/GPS`?
2.  

### 2.2 From the hard-drive to the post-processed data

### 2.3 From the post-processed data to the time series/portal diffusion
